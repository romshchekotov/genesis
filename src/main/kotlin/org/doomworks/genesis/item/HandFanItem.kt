package org.doomworks.genesis.item

import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand

class HandFanItem: Item(settings) {
    override fun useOnEntity(stack: ItemStack, user: PlayerEntity, entity: LivingEntity, hand: Hand): ActionResult {
        if(!user.world.isClient && hand == Hand.MAIN_HAND) {
            val vector = entity.pos.subtract(user.pos)
            val velocity = vector.normalize().multiply(2.0)
            entity.setVelocity(velocity.x, velocity.y, velocity.z)
            return ActionResult.success(user.world.isClient)
        }
        return super.useOnEntity(stack, user, entity, hand)
    }

    companion object {
        val settings = Settings()
    }
}