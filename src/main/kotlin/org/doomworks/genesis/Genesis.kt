package org.doomworks.genesis

import net.fabricmc.api.ModInitializer
import org.doomworks.genesis.registry.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.bernie.geckolib.GeckoLib

// https://www.youtube.com/watch?v=-vYLD74gkJ0 (31:00)

@Suppress("unused")
object Genesis: ModInitializer {
    const val MOD_ID = "genesis"

    val logger: Logger = LoggerFactory.getLogger(MOD_ID)

    override fun onInitialize() {
        GenesisOres.register()
        GenesisItems.register()
        GenesisBlocks.register()
        GenesisVillagers.register()
        GenesisPaintings.register()
        GenesisLootTableOverrides.modifyLootTables()
        GenesisOres.generateOres()

        GeckoLib.initialize()

        logger.info("Genesis has been initialized.")
    }
}