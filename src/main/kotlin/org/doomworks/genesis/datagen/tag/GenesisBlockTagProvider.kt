package org.doomworks.genesis.datagen.tag

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricTagProvider.BlockTagProvider
import net.minecraft.registry.RegistryWrapper
import net.minecraft.registry.tag.BlockTags
import org.doomworks.genesis.registry.GenesisBlocks
import java.util.concurrent.CompletableFuture

@Suppress("DuplicatedCode")
class GenesisBlockTagProvider(output: FabricDataOutput, future: CompletableFuture<RegistryWrapper.WrapperLookup>):
    BlockTagProvider(output, future) {

    override fun configure(arg: RegistryWrapper.WrapperLookup) {
        this.getOrCreateTagBuilder(BlockTags.LOGS)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_LOG.block)

        this.getOrCreateTagBuilder(BlockTags.LOGS_THAT_BURN)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_LOG.block)

        this.getOrCreateTagBuilder(BlockTags.PLANKS)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_PLANKS.block)

        this.getOrCreateTagBuilder(BlockTags.AXE_MINEABLE)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_LOG.block)
            .add(GenesisBlocks.SAKURA_PLANKS.block)
            .add(GenesisBlocks.CHOUCHIN.block)

        this.getOrCreateTagBuilder(BlockTags.PICKAXE_MINEABLE)
            .setReplace(false)
            .add(GenesisBlocks.SPHALERITE_ORE.block)
            .add(GenesisBlocks.DEEPSLATE_SPHALERITE_ORE.block)

        this.getOrCreateTagBuilder(BlockTags.NEEDS_STONE_TOOL)
            .setReplace(false)
            .add(GenesisBlocks.SPHALERITE_ORE.block)
            .add(GenesisBlocks.DEEPSLATE_SPHALERITE_ORE.block)
    }
}