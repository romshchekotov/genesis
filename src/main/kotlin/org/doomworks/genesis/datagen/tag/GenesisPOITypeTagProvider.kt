package org.doomworks.genesis.datagen.tag

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.minecraft.data.server.tag.PointOfInterestTypeTagProvider
import net.minecraft.registry.Registries
import net.minecraft.registry.RegistryWrapper
import net.minecraft.registry.tag.PointOfInterestTypeTags
import org.doomworks.genesis.registry.GenesisVillagers.CHOUCHIN_POI_ID
import org.doomworks.genesis.util.custom
import org.doomworks.genesis.util.key
import java.util.concurrent.CompletableFuture

class GenesisPOITypeTagProvider(output: FabricDataOutput, future: CompletableFuture<RegistryWrapper.WrapperLookup>):
    PointOfInterestTypeTagProvider(output, future) {
    override fun configure(lookup: RegistryWrapper.WrapperLookup) {
        this.getOrCreateTagBuilder(PointOfInterestTypeTags.ACQUIRABLE_JOB_SITE)
            .add(Registries.POINT_OF_INTEREST_TYPE.key(custom(CHOUCHIN_POI_ID)))
    }
}