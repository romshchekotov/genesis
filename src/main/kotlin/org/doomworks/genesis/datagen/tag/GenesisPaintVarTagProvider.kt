package org.doomworks.genesis.datagen.tag

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.minecraft.data.server.tag.PaintingVariantTagProvider
import net.minecraft.registry.Registries
import net.minecraft.registry.RegistryWrapper
import net.minecraft.registry.tag.PaintingVariantTags
import org.doomworks.genesis.registry.GenesisPaintings.SAKURA_PAINTING_ID
import org.doomworks.genesis.util.custom
import org.doomworks.genesis.util.key
import java.util.concurrent.CompletableFuture

class GenesisPaintVarTagProvider(output: FabricDataOutput, future: CompletableFuture<RegistryWrapper.WrapperLookup>):
    PaintingVariantTagProvider(output, future) {
    override fun configure(lookup: RegistryWrapper.WrapperLookup?) {
        this.getOrCreateTagBuilder(PaintingVariantTags.PLACEABLE)
            .add(Registries.PAINTING_VARIANT.key(custom(SAKURA_PAINTING_ID)))
    }
}