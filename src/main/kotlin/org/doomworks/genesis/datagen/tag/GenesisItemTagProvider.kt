package org.doomworks.genesis.datagen.tag

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricTagProvider.ItemTagProvider
import net.minecraft.registry.RegistryWrapper
import net.minecraft.registry.tag.ItemTags
import org.doomworks.genesis.registry.GenesisBlocks
import java.util.concurrent.CompletableFuture

@Suppress("DuplicatedCode")
class GenesisItemTagProvider(output: FabricDataOutput, future: CompletableFuture<RegistryWrapper.WrapperLookup>):
    ItemTagProvider(output, future) {

    override fun configure(arg: RegistryWrapper.WrapperLookup) {
        this.getOrCreateTagBuilder(ItemTags.LOGS)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_LOG.item)

        this.getOrCreateTagBuilder(ItemTags.LOGS_THAT_BURN)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_LOG.item)

        this.getOrCreateTagBuilder(ItemTags.PLANKS)
            .setReplace(false)
            .add(GenesisBlocks.SAKURA_PLANKS.item)
    }
}