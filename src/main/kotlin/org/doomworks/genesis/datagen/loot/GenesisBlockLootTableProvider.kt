package org.doomworks.genesis.datagen.loot

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricBlockLootTableProvider
import net.minecraft.block.Block
import net.minecraft.enchantment.Enchantments
import net.minecraft.item.Item
import net.minecraft.loot.LootPool
import net.minecraft.loot.LootTable
import net.minecraft.loot.condition.BlockStatePropertyLootCondition
import net.minecraft.loot.entry.ItemEntry
import net.minecraft.loot.function.ApplyBonusLootFunction
import net.minecraft.loot.function.ExplosionDecayLootFunction
import net.minecraft.loot.provider.number.ConstantLootNumberProvider
import net.minecraft.predicate.StatePredicate
import net.minecraft.state.property.IntProperty
import org.doomworks.genesis.registry.GenesisBlocks
import org.doomworks.genesis.registry.GenesisItems

class GenesisBlockLootTableProvider(output: FabricDataOutput) : FabricBlockLootTableProvider(output) {
    override fun generate() {
        addDrop(GenesisBlocks.SAKURA_LOG.block)
        addDrop(GenesisBlocks.SAKURA_PLANKS.block)
        addDrop(GenesisBlocks.CHOUCHIN.block)
        addDrop(GenesisBlocks.GOBAN.block)
        addDrop(GenesisBlocks.SPHALERITE_ORE.block)
        addDrop(GenesisBlocks.DEEPSLATE_SPHALERITE_ORE.block)
        addDrop(GenesisBlocks.RICE_CROP, GenesisItems.RICE_SEEDS, GenesisItems.RAW_RICE)
    }

    private fun addDrop(block: Block, seed: Item, crop: Item) {
        val properties = block.defaultState.properties
        val property = properties.find { it.name == "age" } ?:
            throw IllegalArgumentException("Block ${block.name} has no age property")
        val ageProperty: IntProperty = if (property is IntProperty) property else (throw IllegalArgumentException("Block ${block.name} has no age property"))
        val maxValue = ageProperty.values.max() ?: throw IllegalArgumentException("Block ${block.name} has no age property")

        addDrop(block, LootTable.builder().pool(
            LootPool.builder().rolls(ConstantLootNumberProvider.create(1.0F))
                .bonusRolls(ConstantLootNumberProvider.create(0.0F))
                .with(ItemEntry.builder(crop).conditionally(
                    BlockStatePropertyLootCondition.builder(block)
                        .properties(StatePredicate.Builder.create().exactMatch(ageProperty, maxValue))
                ).alternatively(ItemEntry.builder(seed)))
        ).pool(
            LootPool.builder().rolls(ConstantLootNumberProvider.create(1.0F))
                .bonusRolls(ConstantLootNumberProvider.create(0.0F))
                .with(ItemEntry.builder(seed).apply(ApplyBonusLootFunction
                    .binomialWithBonusCount(Enchantments.FORTUNE, 0.5714286F, 3)))
                .conditionally(BlockStatePropertyLootCondition.builder(block)
                    .properties(StatePredicate.Builder.create().exactMatch(ageProperty, maxValue)))
        ).apply(ExplosionDecayLootFunction.builder()))
    }
}