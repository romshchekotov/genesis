@file:Suppress("UnstableApiUsage")

package org.doomworks.genesis.datagen.feature.ore

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricDynamicRegistryProvider
import net.minecraft.registry.RegistryKeys
import net.minecraft.registry.RegistryWrapper
import net.minecraft.world.gen.YOffset
import net.minecraft.world.gen.feature.PlacedFeature
import net.minecraft.world.gen.placementmodifier.CountPlacementModifier
import net.minecraft.world.gen.placementmodifier.HeightRangePlacementModifier
import net.minecraft.world.gen.placementmodifier.SquarePlacementModifier
import org.doomworks.genesis.registry.GenesisOres
import java.util.concurrent.CompletableFuture

class GenesisOreGenerationProvider(
    output: FabricDataOutput?,
    registriesFuture: CompletableFuture<RegistryWrapper.WrapperLookup>
) : FabricDynamicRegistryProvider(output, registriesFuture) {
    override fun getName(): String {
        return "Configured Features for minecraft:ores"
    }

    override fun configure(registries: RegistryWrapper.WrapperLookup, entries: Entries) {
        val oreRegistry = registries.getWrapperOrThrow(RegistryKeys.CONFIGURED_FEATURE)
        entries.addAll(oreRegistry)
        entries.registerOre(GenesisOres.SPHALERITE_ORE, 20, YOffset.getBottom(), YOffset.fixed(64))
    }

    private fun Entries.registerOre(oreGenEntry: GenesisOres.OreGenEntry, count: Int, bottom: YOffset, top: YOffset) {
        val featureRef = add(oreGenEntry.configured, oreGenEntry.feature)
        val placedFeature = PlacedFeature(featureRef, listOf(
            CountPlacementModifier.of(count),
            SquarePlacementModifier.of(),
            HeightRangePlacementModifier.uniform(bottom, top)
        ))
        add(oreGenEntry.placed, placedFeature)
    }
}