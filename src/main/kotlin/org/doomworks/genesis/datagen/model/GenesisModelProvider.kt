package org.doomworks.genesis.datagen.model

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricModelProvider
import net.minecraft.block.Block
import net.minecraft.data.client.*
import net.minecraft.state.property.Properties
import org.doomworks.genesis.block.RiceCropBlock
import org.doomworks.genesis.registry.GenesisBlocks
import org.doomworks.genesis.registry.GenesisItems

@Suppress("unused")
class GenesisModelProvider(output: FabricDataOutput) : FabricModelProvider(output) {
    override fun generateBlockStateModels(blockStateModelGenerator: BlockStateModelGenerator) {
        blockStateModelGenerator.registerLog(GenesisBlocks.SAKURA_LOG.block).log(GenesisBlocks.SAKURA_LOG.block)
        blockStateModelGenerator.registerCrop(GenesisBlocks.RICE_CROP, RiceCropBlock.AGE_6, *(0..6).toList().toIntArray())
        blockStateModelGenerator.registerSimpleCubeAll(GenesisBlocks.SAKURA_PLANKS.block)
        blockStateModelGenerator.registerSimpleCubeAll(GenesisBlocks.SPHALERITE_ORE.block)
        blockStateModelGenerator.registerSimpleCubeAll(GenesisBlocks.DEEPSLATE_SPHALERITE_ORE.block)
    }

    override fun generateItemModels(itemModelGenerator: ItemModelGenerator) {
        itemModelGenerator.register(GenesisItems.BOILED_LEATHER, Models.GENERATED)
        itemModelGenerator.register(GenesisItems.RAW_RICE, Models.GENERATED)
        itemModelGenerator.register(GenesisItems.RAW_ZINC, Models.GENERATED)
        itemModelGenerator.register(GenesisItems.ZINC_INGOT, Models.GENERATED)
    }

    private fun BlockStateModelGenerator.registerCubicLamp(block: Block) {
        val identifier = TexturedModel.CUBE_ALL.upload(block, modelCollector)
        val identifier2 = createSubModel(block, "_on", Models.CUBE_ALL) { TextureMap.all(it) }
        blockStateCollector.accept(
            VariantsBlockStateSupplier.create(block).coordinate(
                BlockStateModelGenerator.createBooleanModelMap(
                    Properties.LIT, identifier2, identifier
                )
            )
        )
    }
}