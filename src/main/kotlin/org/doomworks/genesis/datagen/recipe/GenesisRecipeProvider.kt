package org.doomworks.genesis.datagen.recipe

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricRecipeProvider
import net.minecraft.data.server.recipe.RecipeJsonProvider
import net.minecraft.data.server.recipe.RecipeProvider
import net.minecraft.data.server.recipe.ShapedRecipeJsonBuilder
import net.minecraft.data.server.recipe.ShapelessRecipeJsonBuilder
import net.minecraft.item.Items
import net.minecraft.recipe.book.RecipeCategory
import org.doomworks.genesis.registry.GenesisBlocks
import org.doomworks.genesis.registry.GenesisItems
import java.util.function.Consumer

class GenesisRecipeProvider(output: FabricDataOutput): FabricRecipeProvider(output) {
    override fun generate(exporter: Consumer<RecipeJsonProvider>?) {
        ShapelessRecipeJsonBuilder.create(RecipeCategory.BUILDING_BLOCKS, GenesisBlocks.SAKURA_PLANKS.block, 4)
            .input(GenesisBlocks.SAKURA_LOG.block)
            .criterion("has_sakura_log", RecipeProvider.conditionsFromItem(GenesisBlocks.SAKURA_LOG.block))
            .offerTo(exporter)

        ShapedRecipeJsonBuilder.create(RecipeCategory.TOOLS, GenesisItems.HAND_FAN, 1)
            .pattern("SPS").pattern(" S ")
            .input('S', Items.STICK).input('P', Items.PAPER)
            .criterion("has_paper", RecipeProvider.conditionsFromItem(Items.PAPER))
            .offerTo(exporter)
    }
}