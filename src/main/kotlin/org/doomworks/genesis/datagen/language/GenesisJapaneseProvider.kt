package org.doomworks.genesis.datagen.language

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricLanguageProvider

class GenesisJapaneseProvider(output: FabricDataOutput): FabricLanguageProvider(output, "ja_jp") {
    override fun generateTranslations(translationBuilder: TranslationBuilder) {
        translationBuilder.add("item.genesis.boiled_leather", "沸騰した革")
        translationBuilder.add("item.genesis.hand_fan", "扇子")
        translationBuilder.add("item.genesis.rice_seeds", "米の種")
        translationBuilder.add("item.genesis.raw_rice", "生米")
        translationBuilder.add("item.genesis.raw_zinc", "生鉛")
        translationBuilder.add("item.genesis.zinc_ingot", "鉛インゴット")

        translationBuilder.add("block.genesis.sakura_log", "桜の原木")
        translationBuilder.add("block.genesis.sakura_planks", "桜の板")
        translationBuilder.add("block.genesis.chouchin", "提灯")
        translationBuilder.add("block.genesis.rice_crop", "米の作物")
        translationBuilder.add("block.genesis.sphalerite_ore", "閃亜鉛鉱")
        translationBuilder.add("block.genesis.deepslate_sphalerite_ore", "深板閃亜鉛鉱")
        translationBuilder.add("block.genesis.goban", "碁盤")

        translationBuilder.add("entity.minecraft.villager.japanologist", "日本学者")

        translationBuilder.add("itemGroup.genesis.genesis", "ジェネシス")
    }
}