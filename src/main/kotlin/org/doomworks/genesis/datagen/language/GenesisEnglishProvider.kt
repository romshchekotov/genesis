package org.doomworks.genesis.datagen.language

import net.fabricmc.fabric.api.datagen.v1.FabricDataOutput
import net.fabricmc.fabric.api.datagen.v1.provider.FabricLanguageProvider

class GenesisEnglishProvider(output: FabricDataOutput) : FabricLanguageProvider(output, "en_us") {
    override fun generateTranslations(translationBuilder: TranslationBuilder) {
        translationBuilder.add("item.genesis.boiled_leather", "Boiled Leather")
        translationBuilder.add("item.genesis.hand_fan", "Hand Fan")
        translationBuilder.add("item.genesis.rice_seeds", "Rice Seeds")
        translationBuilder.add("item.genesis.raw_rice", "Raw Rice")
        translationBuilder.add("item.genesis.raw_zinc", "Raw Zinc")
        translationBuilder.add("item.genesis.zinc_ingot", "Zinc Ingot")

        translationBuilder.add("block.genesis.sakura_log", "Sakura Log")
        translationBuilder.add("block.genesis.sakura_planks", "Sakura Planks")
        translationBuilder.add("block.genesis.chouchin", "Chouchin")
        translationBuilder.add("block.genesis.rice_crop", "Rice Crop")
        translationBuilder.add("block.genesis.sphalerite_ore", "Sphalerite")
        translationBuilder.add("block.genesis.deepslate_sphalerite_ore", "Deepslate Sphalerite")
        translationBuilder.add("block.genesis.goban", "Goban")

        translationBuilder.add("entity.minecraft.villager.japanologist", "Japanologist")

        translationBuilder.add("itemGroup.genesis.genesis", "Genesis")
    }
}