package org.doomworks.genesis.registry

import com.google.common.collect.ImmutableSet
import net.fabricmc.fabric.api.`object`.builder.v1.trade.TradeOfferHelper
import net.fabricmc.fabric.api.`object`.builder.v1.world.poi.PointOfInterestHelper
import net.minecraft.block.Block
import net.minecraft.item.ItemStack
import net.minecraft.item.Items
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.registry.entry.RegistryEntry
import net.minecraft.sound.SoundEvents
import net.minecraft.village.TradeOffer
import net.minecraft.village.VillagerProfession
import net.minecraft.world.poi.PointOfInterestType
import org.doomworks.genesis.Genesis
import org.doomworks.genesis.util.custom
import org.doomworks.genesis.util.key

@Suppress("SameParameterValue")
object GenesisVillagers {
    const val CHOUCHIN_POI_ID = "chouchin_poi"
    private val CHOUCHIN_POI = registerPOI(CHOUCHIN_POI_ID, GenesisBlocks.CHOUCHIN.block)
    private val JAPANOLOGIST = registerProfession("japanologist", Registries.POINT_OF_INTEREST_TYPE.key(custom(CHOUCHIN_POI_ID)))

    private fun registerProfession(name: String, type: RegistryKey<PointOfInterestType>): VillagerProfession {
        CHOUCHIN_POI.toString()
        val heldWorkplace: (RegistryEntry<PointOfInterestType>) -> Boolean =
            { station -> station.key.get() == type }
        val prof = VillagerProfession(name, heldWorkplace, heldWorkplace,
            ImmutableSet.of(), ImmutableSet.of(), SoundEvents.ENTITY_VILLAGER_WORK_CARTOGRAPHER)
        return Registry.register(Registries.VILLAGER_PROFESSION, custom(name), prof)
    }

    private fun registerPOI(name: String, block: Block): PointOfInterestType {
        return PointOfInterestHelper.register(custom(name), 1, 1, block.stateManager.states.toSet())
    }

    fun register() {
        Genesis.logger.info("Registering Villagers for {}...", Genesis.MOD_ID)
        registerTrades()
    }

    private fun registerTrades() {
        TradeOfferHelper.registerVillagerOffers(JAPANOLOGIST, 1) {
            it.add { _, _ ->
                TradeOffer(
                    ItemStack(Items.EMERALD, 3),
                    ItemStack(GenesisItems.RAW_RICE, 3),
                    6, 2, 0.02f
                )
            }
        }
    }
}