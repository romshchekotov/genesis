@file:Suppress("UnstableApiUsage")

package org.doomworks.genesis.registry

import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup
import net.minecraft.item.ItemGroup
import net.minecraft.util.Identifier
import org.doomworks.genesis.Genesis

object GenesisGroups {
    val GENESIS: ItemGroup = FabricItemGroup.builder(Identifier(Genesis.MOD_ID, "genesis"))
        .icon { GenesisItems.BOILED_LEATHER.defaultStack }
        .build()
}