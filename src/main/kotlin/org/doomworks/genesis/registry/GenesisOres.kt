package org.doomworks.genesis.registry

import net.fabricmc.fabric.api.biome.v1.BiomeModifications
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors
import net.minecraft.block.Block
import net.minecraft.registry.RegistryKey
import net.minecraft.registry.RegistryKeys
import net.minecraft.registry.tag.BlockTags
import net.minecraft.structure.rule.TagMatchRuleTest
import net.minecraft.world.gen.GenerationStep
import net.minecraft.world.gen.feature.ConfiguredFeature
import net.minecraft.world.gen.feature.Feature
import net.minecraft.world.gen.feature.OreFeatureConfig
import net.minecraft.world.gen.feature.PlacedFeature
import org.doomworks.genesis.Genesis
import org.doomworks.genesis.util.custom

@Suppress("SameParameterValue")
object GenesisOres {
    val SPHALERITE_ORE = registerDualOre(
        GenesisBlocks.SPHALERITE_ORE.block, GenesisBlocks.DEEPSLATE_SPHALERITE_ORE.block,
        "sphalerite_ore", 9)

    @Suppress("unused")
    private fun registerOre(block: Block, key: String, size: Int): OreGenEntry {
        val feature = ConfiguredFeature(Feature.ORE, OreFeatureConfig(
            TagMatchRuleTest(BlockTags.STONE_ORE_REPLACEABLES), block.defaultState, size))
        val configured = RegistryKey.of(RegistryKeys.CONFIGURED_FEATURE, custom(key))
        val placed = RegistryKey.of(RegistryKeys.PLACED_FEATURE, custom(key))

        return OreGenEntry(feature, configured, placed)
    }

    private fun registerDualOre(block: Block, deepslate: Block, key: String, size: Int): OreGenEntry {
        val featureConfig = OreFeatureConfig(
            listOf(
                OreFeatureConfig.createTarget(TagMatchRuleTest(BlockTags.STONE_ORE_REPLACEABLES), block.defaultState),
                OreFeatureConfig.createTarget(TagMatchRuleTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), deepslate.defaultState)
            ), size)
        val feature = ConfiguredFeature(Feature.ORE, featureConfig)
        val configured = RegistryKey.of(RegistryKeys.CONFIGURED_FEATURE, custom(key))
        val placed = RegistryKey.of(RegistryKeys.PLACED_FEATURE, custom(key))
        return OreGenEntry(feature, configured, placed)
    }

    fun register() {
        Genesis.logger.info("Registering Ores for {}", Genesis.MOD_ID)
    }

    fun generateOres() {
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, SPHALERITE_ORE.placed)
    }

    data class OreGenEntry(
        val feature: ConfiguredFeature<OreFeatureConfig, Feature<OreFeatureConfig>>,
        val configured: RegistryKey<ConfiguredFeature<*, *>>,
        val placed: RegistryKey<PlacedFeature>)
}