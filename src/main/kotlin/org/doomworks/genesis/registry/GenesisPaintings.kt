package org.doomworks.genesis.registry

import net.minecraft.entity.decoration.painting.PaintingVariant
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import org.doomworks.genesis.Genesis
import org.doomworks.genesis.util.custom

@Suppress("unused")
object GenesisPaintings {
    const val SAKURA_PAINTING_ID = "sakura_painting"
    private val sakuraPainting = registerPainting(SAKURA_PAINTING_ID, PaintingVariant(32, 25))

    @Suppress("SameParameterValue")
    private fun registerPainting(name: String, painting: PaintingVariant): PaintingVariant {
        return Registry.register(Registries.PAINTING_VARIANT, custom(name), painting)
    }

    fun register() {
        Genesis.logger.info("Registering paintings for {}...", Genesis.MOD_ID)
    }
}