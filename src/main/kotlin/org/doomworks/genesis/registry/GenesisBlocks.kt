package org.doomworks.genesis.registry

import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.ExperienceDroppingBlock
import net.minecraft.block.Material
import net.minecraft.block.PillarBlock
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.util.math.intprovider.UniformIntProvider
import org.doomworks.genesis.Genesis
import org.doomworks.genesis.block.ChouchinBlock
import org.doomworks.genesis.block.GobanBlock
import org.doomworks.genesis.block.RiceCropBlock
import org.doomworks.genesis.util.custom

object GenesisBlocks {
    val SAKURA_LOG = registerBlock("sakura_log", GenesisGroups.GENESIS,
        PillarBlock(FabricBlockSettings.of(Material.WOOD).strength(2.0f)))

    val SAKURA_PLANKS = registerBlock("sakura_planks", GenesisGroups.GENESIS,
        Block(FabricBlockSettings.of(Material.WOOD).strength(2.0f, 3.0f)))

    val CHOUCHIN = registerBlock("chouchin", GenesisGroups.GENESIS, ChouchinBlock())

    val GOBAN = registerBlock("goban", GenesisGroups.GENESIS, GobanBlock())

    val RICE_CROP = registerBlockOnly("rice_crop", RiceCropBlock())

    val SPHALERITE_ORE = registerBlock("sphalerite_ore", GenesisGroups.GENESIS,
        ExperienceDroppingBlock(FabricBlockSettings.of(Material.STONE)
            .strength(4.0f).requiresTool(), UniformIntProvider.create(3, 7)))

    val DEEPSLATE_SPHALERITE_ORE = registerBlock("deepslate_sphalerite_ore", GenesisGroups.GENESIS,
        ExperienceDroppingBlock(FabricBlockSettings.of(Material.STONE)
            .strength(4.0f).requiresTool(), UniformIntProvider.create(3, 7)))

    private fun registerBlock(name: String, tab: ItemGroup, block: Block): BlockItemPair {
        val result = registerBlockOnly(name, block)
        return BlockItemPair(result, registerBlockItem(name, tab, result))
    }

    private fun registerBlockOnly(name: String, block: Block): Block {
        return Registry.register(Registries.BLOCK, custom(name), block)
    }

    private fun registerBlockItem(name: String, tab: ItemGroup, block: Block): Item {
        val item = Registry.register(Registries.ITEM, custom(name), BlockItem(block, Item.Settings()))
        ItemGroupEvents.modifyEntriesEvent(tab).register { entries -> entries.add(item) }
        return item
    }

    fun register() {
        Genesis.logger.info("Registering blocks for {}...", Genesis.MOD_ID)
    }

    data class BlockItemPair(val block: Block, val item: Item)
}