package org.doomworks.genesis.registry

import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents
import net.minecraft.item.AliasedBlockItem
import net.minecraft.item.FoodComponent
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import org.doomworks.genesis.Genesis
import org.doomworks.genesis.item.HandFanItem
import org.doomworks.genesis.util.custom

object GenesisItems {
    val BOILED_LEATHER = registerItem("boiled_leather", GenesisGroups.GENESIS, Item(Item.Settings()))
    val HAND_FAN = registerItem("hand_fan", GenesisGroups.GENESIS, HandFanItem())
    val RICE_SEEDS = registerItem("rice_seeds", GenesisGroups.GENESIS,
        AliasedBlockItem(GenesisBlocks.RICE_CROP, Item.Settings()))
    val RAW_RICE = registerItem("raw_rice", GenesisGroups.GENESIS, Item(Item.Settings().food(
        FoodComponent.Builder().hunger(2).saturationModifier(2f).build())))
    val RAW_ZINC = registerItem("raw_zinc", GenesisGroups.GENESIS, Item(Item.Settings()))
    val ZINC_INGOT = registerItem("zinc_ingot", GenesisGroups.GENESIS, Item(Item.Settings()))

    private fun registerItem(name: String, tab: ItemGroup, item: Item): Item {
        val result = Registry.register(Registries.ITEM, custom(name), item)
        ItemGroupEvents.modifyEntriesEvent(tab).register { entries -> entries.add(result) }
        return result
    }

    fun register() {
        Genesis.logger.info("Registering items for {}...", Genesis.MOD_ID)
    }
}