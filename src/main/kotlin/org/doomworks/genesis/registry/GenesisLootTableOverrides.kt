package org.doomworks.genesis.registry

import net.fabricmc.fabric.api.loot.v2.LootTableEvents
import net.minecraft.block.Blocks
import net.minecraft.loot.LootPool
import net.minecraft.loot.condition.RandomChanceLootCondition
import net.minecraft.loot.entry.ItemEntry
import net.minecraft.loot.function.SetCountLootFunction
import net.minecraft.loot.provider.number.ConstantLootNumberProvider
import net.minecraft.loot.provider.number.UniformLootNumberProvider

object GenesisLootTableOverrides {
    fun modifyLootTables() {
        LootTableEvents.MODIFY.register { _, _, id, builder, source ->
            if(id == Blocks.GRASS.lootTableId && source.isBuiltin) {
                builder.pool(LootPool.builder()
                    .rolls(ConstantLootNumberProvider.create(1f))
                    .conditionally(RandomChanceLootCondition.builder(0.35f))
                    .with(ItemEntry.builder(GenesisItems.RICE_SEEDS))
                    .apply(SetCountLootFunction.builder(UniformLootNumberProvider.create(1f, 2f)))
                    .build())
            }
        }
    }
}