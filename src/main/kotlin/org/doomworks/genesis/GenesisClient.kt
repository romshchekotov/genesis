package org.doomworks.genesis

import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap
import net.minecraft.client.render.RenderLayer
import org.doomworks.genesis.registry.GenesisBlocks

@Suppress("unused")
object GenesisClient : ClientModInitializer {
    override fun onInitializeClient() {
        println("Genesis-Client has been initialized.")
        BlockRenderLayerMap.INSTANCE.putBlock(GenesisBlocks.RICE_CROP, RenderLayer.getCutout())
    }
}