package org.doomworks.genesis.util

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.ShapeContext
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.world.BlockView

@Suppress("OVERRIDE_DEPRECATION")
open class SimpleShapedBlock(settings: Settings, private val shape: VoxelShape): Block(settings) {
    override fun getCollisionShape(state: BlockState, world: BlockView,
                                   pos: BlockPos, context: ShapeContext
    ): VoxelShape = this.shape
    override fun getOutlineShape(state: BlockState, world: BlockView,
                                 pos: BlockPos, context: ShapeContext
    ): VoxelShape = this.shape
    override fun getCameraCollisionShape(state: BlockState, world: BlockView,
                                         pos: BlockPos, context: ShapeContext
    ): VoxelShape = this.shape
    override fun getCullingShape(state: BlockState, world: BlockView,
                                 pos: BlockPos
    ): VoxelShape = this.shape
    override fun getRaycastShape(state: BlockState, world: BlockView,
                                 pos: BlockPos
    ): VoxelShape = this.shape
    override fun getSidesShape(state: BlockState, world: BlockView,
                               pos: BlockPos
    ): VoxelShape = this.shape
}