package org.doomworks.genesis.util

import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.util.Identifier
import org.doomworks.genesis.Genesis

fun custom(path: String): Identifier {
    return Identifier(Genesis.MOD_ID, path)
}

fun <T> Registry<T>.key(id: Identifier): RegistryKey<T> {
    return RegistryKey.of(this.key, id)
}