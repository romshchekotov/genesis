package org.doomworks.genesis

import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator
import org.doomworks.genesis.datagen.feature.ore.GenesisOreGenerationProvider
import org.doomworks.genesis.datagen.language.GenesisEnglishProvider
import org.doomworks.genesis.datagen.language.GenesisJapaneseProvider
import org.doomworks.genesis.datagen.loot.GenesisBlockLootTableProvider
import org.doomworks.genesis.datagen.model.GenesisModelProvider
import org.doomworks.genesis.datagen.recipe.GenesisRecipeProvider
import org.doomworks.genesis.datagen.tag.GenesisBlockTagProvider
import org.doomworks.genesis.datagen.tag.GenesisItemTagProvider
import org.doomworks.genesis.datagen.tag.GenesisPOITypeTagProvider
import org.doomworks.genesis.datagen.tag.GenesisPaintVarTagProvider

@Suppress("unused")
object GenesisDataGen : DataGeneratorEntrypoint {
    override fun onInitializeDataGenerator(dataGenerator: FabricDataGenerator) {
        val resources = dataGenerator.createPack()
        val genericProviders = listOf(
            ::GenesisRecipeProvider,
            ::GenesisModelProvider,
            ::GenesisBlockLootTableProvider,
            ::GenesisJapaneseProvider,
            ::GenesisEnglishProvider)
        genericProviders.forEach { provider -> resources.addProvider { output -> provider.call(output) } }

        val futuristicProviders = listOf(
            ::GenesisItemTagProvider,
            ::GenesisBlockTagProvider,
            ::GenesisPOITypeTagProvider,
            ::GenesisPaintVarTagProvider,
            ::GenesisOreGenerationProvider)

        futuristicProviders.forEach { provider -> resources.addProvider { output, future -> provider.call(output, future) } }
    }
}