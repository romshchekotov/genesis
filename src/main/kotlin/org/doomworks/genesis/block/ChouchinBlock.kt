package org.doomworks.genesis.block

import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.Material
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Items
import net.minecraft.state.StateManager
import net.minecraft.state.property.Properties
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.world.World
import org.doomworks.genesis.util.SimpleShapedBlock

class ChouchinBlock: SimpleShapedBlock(settings, shape) {
    init {
        defaultState = defaultState.with(Properties.LIT, false)
    }

    override fun appendProperties(builder: StateManager.Builder<Block, BlockState>) {
        builder.add(Properties.LIT)
    }

    /**
     * Can override the deprecated method, as it's not really 'deprecated'
     * @see <a href="https://www.reddit.com/r/fabricmc/comments/r8zi36/deprecation/">Fabric Deprecation</a>
     */
    @Suppress("OVERRIDE_DEPRECATION")
    override fun onUse(state: BlockState, world: World, pos: BlockPos,
                       player: PlayerEntity, hand: Hand, hit: BlockHitResult
    ): ActionResult {
        if(!world.isClient) {
            val handStack = player.getStackInHand(hand)
            if(handStack.isOf(Items.TORCH) && !state.get(Properties.LIT)) {
                if(!player.isCreative) handStack.decrement(1)
                world.setBlockState(pos, state.with(Properties.LIT, true),
                    NOTIFY_ALL or REDRAW_ON_MAIN_THREAD)
            } else if(state.get(Properties.LIT)) {
                player.inventory.insertStack(Items.TORCH.defaultStack)
                world.setBlockState(pos, state.with(Properties.LIT, false),
                    NOTIFY_ALL or REDRAW_ON_MAIN_THREAD)
            }


            return ActionResult.success(world.isClient)
        }
        return ActionResult.PASS
    }

    companion object {
        val settings: Settings = Settings.of(Material.REDSTONE_LAMP)
            .nonOpaque().requiresTool().strength(0.3f)
            .luminance { state -> if (state.get(Properties.LIT)) 15 else 0 }

        val shape: VoxelShape = createCuboidShape(
            2.0, 0.0, 2.0,
            14.0, 16.0, 14.0)
    }
}