package org.doomworks.genesis.block

import net.minecraft.block.Material
import net.minecraft.util.shape.VoxelShape
import org.doomworks.genesis.util.SimpleShapedBlock

class GobanBlock: SimpleShapedBlock(settings, shape) {


    companion object {
        val settings: Settings = Settings.of(Material.WOOD)
            .nonOpaque().requiresTool().strength(0.3f)

        val shape: VoxelShape = createCuboidShape(
            1.0, 0.0, 1.0,
            15.0, 4.0, 15.0)
    }
}