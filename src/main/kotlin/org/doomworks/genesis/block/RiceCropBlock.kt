package org.doomworks.genesis.block

import net.minecraft.block.*
import net.minecraft.item.ItemConvertible
import net.minecraft.state.StateManager
import net.minecraft.state.property.IntProperty
import org.doomworks.genesis.registry.GenesisItems

class RiceCropBlock : CropBlock(settings) {
    override fun getMaxAge() = 6

    override fun getAgeProperty(): IntProperty = AGE_6
    override fun getSeedsItem(): ItemConvertible = GenesisItems.RICE_SEEDS
    override fun appendProperties(builder: StateManager.Builder<Block, BlockState>) { builder.add(AGE_6) }
    companion object {
        val AGE_6: IntProperty = IntProperty.of("age", 0, 6)
        val settings: Settings = Settings.copy(Blocks.WHEAT)
    }
}