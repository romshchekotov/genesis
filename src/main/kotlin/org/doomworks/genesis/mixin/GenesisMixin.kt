package org.doomworks.genesis.mixin

import net.minecraft.client.gui.screen.TitleScreen
import org.doomworks.genesis.Genesis
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Inject
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo

@Mixin(TitleScreen::class)
class GenesisMixin {
    @Inject(at = [At("HEAD")], method = ["init()V"])
    private fun init(callbackInfo: CallbackInfo) {
        Genesis.logger.info("Genesis Mixin has been initialized [{}]", callbackInfo.id)
    }
}