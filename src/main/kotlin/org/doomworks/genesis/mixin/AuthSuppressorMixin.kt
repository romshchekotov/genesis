@file:Suppress("SpellCheckingInspection")

package org.doomworks.genesis.mixin

import net.minecraft.client.MinecraftClient
import org.slf4j.Logger
import org.spongepowered.asm.mixin.Mixin
import org.spongepowered.asm.mixin.injection.At
import org.spongepowered.asm.mixin.injection.Redirect

@Mixin(MinecraftClient::class)
class AuthSuppressorMixin {
    @Redirect(
        method = ["createUserApiService"],
        at = At(value = "INVOKE", target="Lorg/slf4j/Logger;error(Ljava/lang/String;Ljava/lang/Throwable;)V"))
    private fun errorProxy(logger: Logger, s: String, @Suppress("UNUSED_PARAMETER") ignored: Throwable) {
        logger.error("Failed to authenticate with Mojang servers: $s")
    }
}