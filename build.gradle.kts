@file:Suppress("SpellCheckingInspection")

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("fabric-loom")
    kotlin("jvm").version(System.getProperty("kotlin_version"))
}

base { archivesName.set(project.extra["mod_id"] as String) }
version = project.extra["mod_version"] as String
group = project.extra["maven_group"] as String

repositories {
    maven("https://dl.cloudsmith.io/public/geckolib3/geckolib/maven/")
}

dependencies {
    minecraft("com.mojang", "minecraft", project.extra["minecraft_version"] as String)
    mappings("net.fabricmc", "yarn", project.extra["yarn_mappings"] as String, null, "v2")
    modImplementation("net.fabricmc", "fabric-loader", project.extra["loader_version"] as String)
    modImplementation("net.fabricmc.fabric-api", "fabric-api", project.extra["fabric_version"] as String)
    modImplementation("net.fabricmc", "fabric-language-kotlin", project.extra["fabric_language_kotlin_version"] as String)

    modImplementation("software.bernie.geckolib", "geckolib-fabric-${project.extra["minecraft_version"]}", project.extra["geckolib_version"] as String)
}

loom {
    runs {
        create("datagen") {
            inherit(getByName("client"))
            name("Data Generation")
            vmArgs(
                "-Dfabric-api.datagen",
                "-Dfabric-api.datagen.output-dir=${file("src/main/generated")}",
                "-Dfabric-api.datagen.modid=${project.extra["mod_id"] as String}",
            )
            runDir("build/datagen")
        }
    }
}

sourceSets {
    main {
        resources {
            srcDirs += file("src/main/generated")
        }
    }
}

tasks {
    val javaVersion = JavaVersion.toVersion((project.extra["java_version"] as String).toInt())

    withType<JavaCompile> {
        options.encoding = "UTF-8"
        sourceCompatibility = javaVersion.toString()
        targetCompatibility = javaVersion.toString()
        options.release.set(javaVersion.toString().toInt())
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = javaVersion.toString()
    }

    jar {
        from("LICENSE") {
            rename { "${it}_${base.archivesName.get()}" }
        }
    }

    processResources {
        filesMatching("fabric.mod.json") {
            expand(mutableMapOf(
                "version" to project.extra["mod_version"] as String,
                "fabricloader" to project.extra["loader_version"] as String,
                "fabric_api" to project.extra["fabric_version"] as String,
                "fabric_language_kotlin" to project.extra["fabric_language_kotlin_version"] as String,
                "minecraft" to project.extra["minecraft_version"] as String,
                "java" to project.extra["java_version"] as String
            ))
        }
        filesMatching("*.mixins.json") {
            expand(mutableMapOf(
                "java" to project.extra["java_version"] as String
            ))
        }
    }

    java {
        toolchain.languageVersion.set(JavaLanguageVersion.of(javaVersion.toString()))
        sourceCompatibility = javaVersion
        targetCompatibility = javaVersion
        withSourcesJar()
    }

    register("mergeResources") {
        doLast {
            val resources = file("src/main/resources")
            val generated = file("src/main/generated")
            generated.copyRecursively(resources, true)
        }
    }

    register("cleanGenerated") {
        doLast {
            val generated = file("src/main/generated")
            generated.walkTopDown().forEach {
                if (it.isFile) {
                    val resource = file("src/main/resources/${it.relativeTo(generated)}")
                    if (resource.exists()) {
                        resource.delete()
                    }
                }
            }
            // Walk the directory tree backwards to delete empty directories in 'resources'
            val resources = file("src/main/resources")
            resources.walkBottomUp().forEach {
                if (it.isDirectory && it.listFiles().isEmpty()) {
                    it.delete()
                }
            }
        }
    }

    getByName("runDatagen") {
        dependsOn("cleanGenerated")
        doFirst {
            val generated = file("src/main/generated")
            generated.deleteRecursively()
        }
        finalizedBy("mergeResources")
    }
}
